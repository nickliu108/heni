import React from "react";
import { render, fireEvent, getByLabelText, screen } from '@testing-library/react'
import Pagination from "./index";

const updatePage = jest.fn();

describe('Pagination component', () => {
    it('should render Pagination component', () => {
        const Component = render(<Pagination updatePage={updatePage} currentPage={1} totalPages={3} />)
        expect(Component.baseElement).toMatchSnapshot();
    })

    it('should call updatePage on click next page', () => {
        const {getByText} = render(<Pagination updatePage={updatePage} currentPage={1} totalPages={3} />)
        fireEvent.click(getByText('next page'))
        expect(updatePage).toHaveBeenCalledTimes(1)
        expect(updatePage).toHaveBeenCalledWith(2)
    })

    it('should call updatePage on click previous page', () => {
        const {getByText} = render(<Pagination updatePage={updatePage} currentPage={2} totalPages={3} />)
        fireEvent.click(getByText('previous page'))
        expect(updatePage).toHaveBeenCalledTimes(1)
        expect(updatePage).toHaveBeenCalledWith(1)
    })

    it('should NOT call updatePage when clicking previous page on the first page', () => {
        const {getByText} = render(<Pagination updatePage={updatePage} currentPage={1} totalPages={3} />)
        fireEvent.click(getByText('previous page'))
        expect(updatePage).toHaveBeenCalledTimes(0)
    })

    it('should NOT call updatePage when clicking next page on the last page', () => {
        const {getByText} = render(<Pagination updatePage={updatePage} currentPage={3} totalPages={3} />)
        fireEvent.click(getByText('next page'))
        expect(updatePage).toHaveBeenCalledTimes(0)
    })
})
