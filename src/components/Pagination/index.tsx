import React, { FC } from 'react';
import { Button } from '@mui/material';
import './index.css'
import { PropsType } from './types'

const Pagination:FC<PropsType> = ({
    updatePage,
    currentPage,
    totalPages
}) =>{

    const prevousPage = () => {
        if (currentPage > 1) {
            updatePage(currentPage - 1)
        }
    }

    const nextPage = () => {
        if (currentPage < totalPages) {
            updatePage(currentPage + 1)
        }
    }


    return (
        <div className="pagination">
            <Button variant="contained" disabled={currentPage <= 1} onClick={prevousPage}>previous page</Button>
            <Button variant="contained" disabled={currentPage >= totalPages} onClick={nextPage}>next page</Button>
        </div>
    )
}

export default Pagination