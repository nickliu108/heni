export type PropsType = {
    currentPage: number
    totalPages: number
    updatePage: (currentPage: number) => void
}