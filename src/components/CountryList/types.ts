export type Country = {
    name: string
    code: string
    native: string
    phone: string
    continent: {
        name: string
    }
    capital: string
    currency: string
    emoji: string

}

export type Countries = Array<Country> | []

export type PropsType = {
    countries: Countries
    loading: boolean
}