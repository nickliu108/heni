import React, { FC } from 'react';
import { Grid } from '@mui/material';
import { PropsType, Country } from './types'

const CountryList:FC<PropsType> = ({countries = [], loading = false}) => {
    return  loading ? ( <div>loading...</div>) :
    countries.length > 0 
        ?  
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            {
                countries.map((country:Country, index:number) => (
                    <Grid item xs={2} sm={4} md={4} key={index}>
                        <div>Country Name: {country.name}</div>
                        <div>Calling code: {country.phone}</div>
                        <div>Capital: {country.capital}</div>
                        <div>{country.emoji}</div>
                    </Grid>
                ))
            }
        </Grid>
    : (<div></div>)
}

export default CountryList