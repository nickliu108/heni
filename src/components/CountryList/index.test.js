import React from "react";
import { render, fireEvent, getByLabelText, screen } from '@testing-library/react'
import CountryList from "./index";

const countries = [
    {
        name:'country 1',
        phone:1234,
        capital: 'captial 1',
        emoji: 'emoji1',
    },
    {
        name:'country 2',
        phone:1234,
        capital: 'captial 2',
        emoji: 'emoji2'
    },
    {
        name:'country 3',
        phone:1234,
        capital: 'captial 3',
        emoji: 'emoji3'
    }
]

describe('CountryList component', () => {
    it('should render CountryList component', () => {
        const Component = render(<CountryList countries={countries} />)
        expect(Component.baseElement).toMatchSnapshot();
    })

    it('should render CountryList component with loading state', () => {
        const Component = render(<CountryList countries={countries} loading={true}/>)
        expect(Component.baseElement).toMatchSnapshot();
    })
})
