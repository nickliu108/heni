import React, { useEffect, useReducer } from 'react';
import { gql, useQuery } from '@apollo/client'
import CountryList from '../CountryList'
import Pagination from '../Pagination'
import Paper from '@mui/material/Paper';
import { GET_COUNTRIES } from './graphql'
import { StateType, ActionType } from './types'

export const reducer = (state: StateType, action: ActionType) => {
    switch (action.type) {
        case 'SET_COUNTRIES':
            return {
                ...state,
                dispalyCountries: action.dispalyCountries
            };
        case 'SET_COUNTRIES_TOTALPAGES':
            return {
                ...state,
                dispalyCountries: action.dispalyCountries,
                totalPages: action.totalPages
            };
        case 'SET_CURRENTPAGE':
            return {
                ...state,
                currentPage: action.currentPage
            }
        case 'SET_TOTALPAGES':
            return {
                ...state,
                totalPages: action.totalPages
            }
    }
}

function Container() {
    const [state, dispatch] = useReducer(reducer, {
        pageSize: 25,
        currentPage: 1,
        totalPages: 1,
        dispalyCountries: []
    });

    const { loading, data, error} = useQuery(GET_COUNTRIES) ;

    useEffect(() => {
        if(data?.countries?.length > 0) {
            const totalPages = Math.ceil(data.countries.length / state.pageSize);
            dispatch({
                type: 'SET_COUNTRIES_TOTALPAGES',
                totalPages,
                dispalyCountries: data.countries.slice(0, state.pageSize - 1)
            });
        }
    }, [data])

    useEffect(() => {
        const startIndex = state.currentPage * state.pageSize;
        const endIndex = startIndex + state.pageSize - 1;

        dispatch({
            type: 'SET_COUNTRIES',
            dispalyCountries: data?.countries?.slice(startIndex, endIndex)
        })
    }, [state.currentPage])

    const updatePage = (currentPage: number) => {
        dispatch({
            type: 'SET_CURRENTPAGE',
            currentPage
        })
    }

    return (
        <Paper 
            sx={{
                p: 2,
                margin: 'auto',
                maxWidth: 1080,
                flexGrow: 1,
                backgroundColor: (theme) =>
                theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
            }}>
            {
                error ? (<div>{error.message}</div>) : (
                    <>
                        <Pagination currentPage={state.currentPage} totalPages={state.totalPages} updatePage={updatePage} />
                        <CountryList countries={state.dispalyCountries} loading={loading}/>
                    </>
                )
                
            }
        </Paper>
    );
}

export default Container;
