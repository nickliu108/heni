import { gql } from '@apollo/client'
export const GET_COUNTRIES = gql`
    query getCountries {
        countries {
            code
            name
            emoji
            emojiU
            native
            phone
            continent {
                name
            }
            capital
            currency
            languages {
                name
            }
        }
    }
`;
