import { render, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import  Container  from './index';
import { GET_COUNTRIES } from './graphql'

const mocks = [
    {
      request: {
        query: GET_COUNTRIES
      },
      result: {
        data: {
            countries: [
                {
                    name:'country 1',
                    phone:1234,
                    capital: 'captial 1',
                    emoji: 'emoji1',
                },
                {
                    name:'country 2',
                    phone:1234,
                    capital: 'captial 2',
                    emoji: 'emoji2'
                },
                {
                    name:'country 3',
                    phone:1234,
                    capital: 'captial 3',
                    emoji: 'emoji3'
                }
            ]
        }
      }
    }
  ]


describe('App', () => {
    it('should render country list', async () => {
        const { container } = render(
            <MockedProvider mocks={mocks}>
              <Container />
            </MockedProvider>
          );
          await screen.findByText('Country Name: country 1')
          expect(container).toMatchSnapshot();
    })
  
    it('should render with error',  async () => {
        const errorMocks = [
            {
              request: {
                query: GET_COUNTRIES
              },
              result: {
                data: {
                    countries: [
                        {
                            name:'country 1',
                            phone:1234,
                            capital: 'captial 1',
                            emoji: 'emoji1',
                        },
                        {
                            name:'country 2',
                            phone:1234,
                            capital: 'captial 2',
                            emoji: 'emoji2'
                        },
                        {
                            name:'country 3',
                            phone:1234,
                            capital: 'captial 3',
                            emoji: 'emoji3'
                        }
                    ]
                },
                errors: [
                    {
                        message: 'an error occurred'
                    }
                ]
              }
            }
          ]

          const { container } = render(
            <MockedProvider mocks={errorMocks}>
              <Container />
            </MockedProvider>
          );
          await screen.findByText('an error occurred')
          expect(container).toMatchSnapshot();
    })
})