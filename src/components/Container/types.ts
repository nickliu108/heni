type Country = {
    name: string
    code: string
    native: string
    phone: string
    continent: {
        name: string
    }
    capital: string
    currency: string
    emoji: string

}

export type Countries = Array<Country> | []

export type StateType = {
    pageSize: number
    totalPages: number
    currentPage: number
    dispalyCountries: Countries
}

export type ActionType = {
    type: 'SET_COUNTRIES'
    dispalyCountries: Countries
} | {
    type: 'SET_CURRENTPAGE'
    currentPage: number
} | {
    type: 'SET_TOTALPAGES'
    totalPages: number
} | {
    type: 'SET_COUNTRIES_TOTALPAGES'
    totalPages: number
    dispalyCountries: Countries
}