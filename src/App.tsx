import React from 'react';
import './App.css';
import Container from './components/Container'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider
} from "@apollo/client";


const client = new ApolloClient({
  uri: 'https://countries.trevorblades.com',
  cache: new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Container />
    </ApolloProvider>
    
  );
}

export default App;
